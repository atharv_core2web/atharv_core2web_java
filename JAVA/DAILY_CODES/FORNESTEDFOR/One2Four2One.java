class One2Four2One{
	public static void main(String[] args){
		for(int i = 1 ; i <= 4 ; i++){
			int num1 = 1;
			int num2 = 4;
			for(int j = 1 ; j <= 4 ; j++){
				if(i % 2 == 0){
					System.out.print(num2 + " ");
				}
				else{
					System.out.print(num1+ " ");
				}
				num1++;
				num2--;
			}
			System.out.println();
		}
	}
}
