import java.io.*;

class Program6{
        public static void main(String[] args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader (System.in));
                System.out.print("Enter Rows : ");
                int row = Integer.parseInt(br.readLine());
                for(int i = 1; i <= row ; i++){
                        char ch = 'a';
			int num = 1 ;
                        for(int j = i ; j <= row ; j++){
				if((i + j) % 2 != 0){
					System.out.print(ch + " ");
					ch++;
				}
				else{
					System.out.print(num + " ");
					num++;
				}
				
				//num++;
				//ch++;
			}
			System.out.println();
		}
	}
}
