class Parent{


}

class Child extends Parent{


}

class Demo{
	
	void fun(Parent obj){

		System.out.println("In - fun Parent");

	}
	void fun(Child obj){

		System.out.println("In - child Parent");

	}
}

class Outer{

	public static void main(String[] s){
	
		Demo obj = new Demo();

		obj.fun(new Parent());
		obj.fun(new Child());
	}
}
