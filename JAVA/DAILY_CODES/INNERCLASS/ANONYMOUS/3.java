class Demo{

	void fun(Object x){

		System.out.println("In Fun");

	}
}

class Outer{

	public static void main(String[] s){

		Demo obj = new Demo();

		obj.fun(10);
		obj.fun(10.5);
		obj.fun("Atharv");
		obj.fun(new String("Atharv"));
		obj.fun(10.5f);
	}
}
