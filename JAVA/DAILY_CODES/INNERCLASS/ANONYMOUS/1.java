class Parent{

	void marry(){

		System.out.println("ABC");
	}
}

class Demo{

	void fun(Parent obj){

		obj.marry();
	}
}

class Outer{

	public static void main(String[] s){

		Demo obj1 = new Demo();

		obj1.fun(new Parent(){
			
			void marry(){
				System.out.println("XYZ");
			}
		});
	}

}
