import java.io.*;

class cntFileDir{
	
	public static void main(String[] s)throws IOException {

		//CREATING FILES
		File fObj1 = new File("File_1");
	       	File fObj2 = new File("File_2");
       		File fObj3 = new File("File_3");
		File fObj4 = new File("File_4");
		File fObj5 = new File("File_5");
		
		fObj1.createNewFile();                
		fObj2.createNewFile();                
		fObj3.createNewFile(); 
	        fObj4.createNewFile();
		fObj5.createNewFile();
	
		//CREATING FOLDERS
		File dirObj1 = new File("Folder_1");
		File dirObj2 = new File("Folder_2");
		File dirObj3 = new File("Folder_3");
	
		dirObj1.mkdir();
		dirObj2.mkdir();
		dirObj3.mkdir();

		File fObj = new File("/home/atharv/atharv_core2web_java/JAVA/DAILY_CODES/FILEHANDLING");

		//COUNTIING FILES AND FOLDERS
		int fileCnt = 0;
		int dirCnt = 0;

		File[] names = fObj.listFiles();
	   
		for(int i = 0 ; i< names.length ; i++){
			
			if(names[i].isDirectory()){
				dirCnt++;
			}
			else if(names[i].isFile()) {
				fileCnt++;
			}

			//System.out.println(names[i]);
		}	
		System.out.println("Total files in present Working directory : " + fileCnt);
		System.out.println("Total directories in present Working directory : " + dirCnt);
	}
}
