import java.util.*;

class Program5{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enters Rows :");
		int row = sc.nextInt();
		int col = 0;
		int num = 0;
		for(int i = 1 ; i <= (2 * row) - 1 ; i++){
			
			if(i <= row){
				col = i;	
				num = row - i + 1;		
			}
			else{					
				col = (2 * row) - i;
				num = i - row + 1 ;
			}
			for(int j = 1 ; j <= col ; j++){
				System.out.print((char)(64 + num) + "\t");
			}
			//num--;
			System.out.println();
		}
	}
}
