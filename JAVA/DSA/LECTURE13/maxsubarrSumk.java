import java.util.*;

//OPTIMIZED
//TC : O(N)
//SC : O(1)
/*
class maxsubarrSumk{

        public static void main(String[] s){

                Scanner sc = new Scanner(System.in);

                int arr[] = new int[]{-3,4,-2,5,3,-2,8,2,1,4};

                System.out.println("Enter length of subarray : ");
                int k = sc.nextInt();

                int N = arr.length;

		int start = 0;
		int end = k - 1;

		int maxSum = Integer.MIN_VALUE;
		
		while(end < N){

			int sum = 0;
			for(int i = start ; i <= end ; i++){
			       sum += arr[i];
			}	      
		       	
			if(sum > maxSum)
				maxSum = sum;	
		
			start++;
			end++;
		}

		System.out.println(maxSum);
	}
}
*/

//USING PREFIX SUM
//TC : O(N)
//SC : O(N)

class maxsubarrSumk{

        public static void main(String[] s){

                Scanner sc = new Scanner(System.in);

                int arr[] = new int[]{-3,4,-2,5,3,-2,8,2,1,4};

                System.out.println("Enter length of subarray : ");
                int k = sc.nextInt();

                int N = arr.length;

		int preSum[] = new int[N];
		preSum[0] = arr[0];

                int start = 0;
                int end = k - 1;

		int maxSum = Integer.MIN_VALUE;
		
		//TC : O(N)
		for(int i = 1 ; i < N ; i++){

			preSum[i] = preSum[i - 1] + arr[i];
		}
		
		//TC : O(N + k)
		while(end < N){
			int sum = 0;

			if(start == 0)
				sum = preSum[end];
			else
				sum = preSum[end] - preSum[start - 1];
		

		if(sum > maxSum){
                
			maxSum = sum;
		}

		start++;
		end++;
		}

		System.out.println(maxSum);
	}
}
