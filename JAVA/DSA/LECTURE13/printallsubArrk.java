import java.util.*;

//BRUTEFORCE
//TC: O(N^2)
/*class printallsubArrk{
	
	public static void main(String[] s){

		Scanner sc = new Scanner(System.in);

		int arr[] = new int[]{-3,4,-2,5,3,-2,8,2,1,4};
		 
		System.out.println("Enter length of subarray : ");
		int k = sc.nextInt();

		int N = arr.length;

		int cnt = 0;

		for(int i = 0  ; i < N ; i++){
			if((i + k) <= N){ 
			for(int j = i ; j < i + k ; j++){
		
				System.out.print(arr[j] + " 	");
				}
			}
			System.out.println();
		}
	}

}*/


//OPTIMIZED
//TC:O(N)


class printallsubArrk{

        public static void main(String[] s){

                Scanner sc = new Scanner(System.in);

                int arr[] = new int[]{-3,4,-2,5,3,-2,8,2,1,4};

                System.out.println("Enter length of subarray : ");
                int k = sc.nextInt();

                int N = arr.length;

                int cnt = 0;
		int end = k - 1;

		while(end < N){

			cnt++;
			end++;
		}

		System.out.println(cnt);
	}
}


//SOP(N - K - 1);
