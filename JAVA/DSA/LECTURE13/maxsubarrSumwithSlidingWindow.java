import java.util.*;

class maxsubarrSumwithSlidingWindow{

	public static void main(String[] s){

                Scanner sc = new Scanner(System.in);

                int arr[] = new int[]{-3,4,-2,5,3,-2,8,2,1,4};

                System.out.println("Enter length of subarray : ");
                int k = sc.nextInt();

                int N = arr.length;

		int start = 0;
		int end = k - 1;

		int sum = 0;

		//CALCULATE SUM OF FIRST SUB-ARRAY
		for(int i = start ; i <= end ; i++){
		
			sum += arr[i];
		}	

		start = 1;
		end = k;
		
		//SUM IS DIRECTLY PUT IN MAXSUM
		int maxSum = sum;

		while(end < N){
			
			//REMOVE FIRST ELEMENT OF PREVIOUS SUBARRAY AND ADD LAST ELEMENT OF CURRENT SUB ARRAY 
			sum = sum - arr[start - 1] + arr[end];

			if(sum > maxSum)
				maxSum = sum;
			
			start++;
			end++;
		}

		System.out.println(maxSum);
	}
}
