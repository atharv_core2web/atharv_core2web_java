//BUTEFORCE 
//TC : O(N^3) SC : O(1)

/*
class printmaxsumofsubArr{

        public static void main(String[] s){

                int arr[] = new int[]{-2,1,-3,4,-1,2,1,-5,4};

                int N = arr.length;

		int maxSum = Integer.MIN_VALUE;
                for(int i = 0 ; i < N ; i++){
                        for(int j = i ; j < N ; j++){
                        int sum = 0;
                                for(int k = i ; k <= j ; k++){
                                        sum += arr[k];                           	

				if(maxSum < sum)
					maxSum = sum;
				}
                        }
                }
                System.out.println(maxSum);
        }
}*/

//CARRYFORWARD
//TC : O(N^2) SC : O(1)

/*
class printmaxsumofsubArr{

        public static void main(String[] s){

                int arr[] = new int[]{-2,1,-3,4,-1,2,1,-5,4};

                int N = arr.length;

                int maxSum = Integer.MIN_VALUE;
                for(int i = 0 ; i < N ; i++){
                        int sum = 0;
                        for(int j = i ; j < N ; j++){
                        	sum += arr[j];
		
			 if(maxSum < sum)
                                        maxSum = sum;
                                
			}
		}
		System.out.println(maxSum);
	}
}*/


//PREFIX SUM
//TC : O(N^2) SC : O(1)


class printmaxsumofsubArr{

        public static void main(String[] s){

                int arr[] = new int[]{-2,1,-3,4,-1,2,1,-5,4};
	
                int N = arr.length;

       		int maxSum = Integer.MIN_VALUE;

		int preSum[] = new int[N];
		preSum[0] = arr[0];

		for(int i = 1 ; i < N ; i++){
			preSum[i] = preSum[i - 1] + arr[i];
		}

		for(int i = 0 ; i < N ; i++){
                        int sum = 0;
			for(int j = 0 ; j < N ; j++){
				if(i == 0)
					sum = preSum[0];
				else
					sum = preSum[j] - preSum[i - 1];

				if(maxSum < sum)
                                        maxSum = sum;

                        }
                }
                System.out.println(maxSum);
	}
}

