//TC : 0(M * N)

class printallDiagonal {

        public static void main(String[] s){

                int arr[][] = new int[][]{{1,2,3,4,5,6} , {7,8,9,10,11,12} , {13,14,15,16,17,18} , {19,20,21,22,23,24} , {25,26,27,28,29,30}};

		int M = arr[0].length;
		int N = arr.length;

		for(int j = (M - 1) ; j >= 0 ; j--){

			int i = 0;
			int y = j;

			/*while(i < N && y >= 0){
				System.out.print(arr[i][y] + " ");
				i++;
				y--;
			}*/
			for(; i < N && y >= 0 ; i++ , y--){
				System.out.print(arr[i][y] + " ");
			}
			System.out.println();
		}
	}
}

