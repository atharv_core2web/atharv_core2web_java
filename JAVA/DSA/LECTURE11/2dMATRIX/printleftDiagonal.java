class printleftDiagonal {

	public static void main(String[] s){

		int arr[][] = new int[][]{{1,2,3},{4,5,6},{7,8,9}};

		//TC : O(N^2)
		/*for(int i = 0 ; i < arr.length ; i++){
			for(int j = 0 ; j < arr[i].length ; j++){
				if(i == j)
					System.out.print(arr[i][j] + "	");

			}
		}*/
		

		//TC : O(N)
		for(int i = 0 ; i < arr.length ; i++){
					System.out.print(arr[i][i] + "	");
	
		}
	}
}
