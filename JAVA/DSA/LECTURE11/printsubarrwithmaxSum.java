class printsubarrwithmaxSum {

        public static void main(String[] s){

                int arr[] = new int[]{-2,1,-3,4,-1,2,1,-5,4};
               // int arr[] = new int[]{-2,1,-3};

		int N = arr.length;

                int maxSum = Integer.MIN_VALUE;

                int sum = 0;
		
		int startIndex = -1;
                int endIndex = -1;
                int currentIndex = -1;

                for(int i = 0 ; i < N ; i++){

			if(sum == 0)
				currentIndex = i;

                        sum += arr[i];

                        if(sum > maxSum){
                                maxSum = sum;
				startIndex = currentIndex;
				endIndex = i;
			}

                        if(sum < 0)
                                sum = 0;
                        
		}
                
		for(int j = startIndex ; j <= endIndex ; j++){
                               System.out.print(arr[j] + " ");
                }
        }
}
