class kadanesAlgo {

        public static void main(String[] s){

                int arr[] = new int[]{-2,1,-3,4,-1,2,1,-5,4};

		int N = arr.length;

                int maxSum = Integer.MIN_VALUE;

                int sum = 0;

                for(int i = 0 ; i < N ; i++){

                        sum += arr[i];

                        if(sum > maxSum)
                                maxSum = sum;

                        if(sum < 0)
                                sum = 0;
                }

		System.out.println(maxSum);

        }
}
