import java.util.Arrays;

class QuickSort {
    // Swap method using a temporary variable
    static void swap(int arr[], int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
    
    // Partition method that selects the last element as pivot
    static int partition(int arr[], int start, int end) {
        int pivot = arr[end];
        int j = start - 1;
        
        for (int i = start; i < end; i++) {
            if (arr[i] <= pivot) {
                j++;
                swap(arr, i, j);
            }
        }
        swap(arr, j + 1, end);
        return j + 1;
    }
    
    // Main quickSort recursive method
    static void quickSort(int arr[], int start, int end) {
        if (start < end) {
            int pivot = partition(arr, start, end);
            quickSort(arr, start, pivot - 1);
            quickSort(arr, pivot + 1, end);
        }
    }
    
    public static void main(String[] args) {
        int arr[] = new int[]{6, 2, 10, 9, 1, 0, -1};
        System.out.println("Original array: " + Arrays.toString(arr));
        
        quickSort(arr, 0, arr.length - 1);
        
        System.out.println("Sorted array: " + Arrays.toString(arr));
    }
}
