class mergerSortPractice{

	static void divide(int arr[],int start,int end){
	
		if(start < end){
		
			int mid = start + (end - start) / 2;
			
			divide(arr , start , mid);
			divide(arr , mid + 1 , end);
			merge(arr,start,mid,end);
		}

	}

	static void merge(int arr[] , int start , int mid, int end){
	
		int n1 = mid - start + 1;
		int n2 = end - mid;

		int leftArr[] = new int[n1];
		int rightArr[] = new int[n2];

		for(int i = 0 ; i < n1 ; i++){
		
			leftArr[i] = arr[i + start];
		}
		
		for(int i = 0 ; i < n2 ; i++){

                        rightArr[i] = arr[i + mid + 1];
                }

		int i = 0 , j = 0 , k = start;
	       	
		while(i<n1 && j<n2){
		//while(i<leftArr.length && j<rightArr.length){
		
			if(leftArr[i] < rightArr[j]){
			
				arr[k] = leftArr[i];
				i++;
			}else{

				arr[k] = rightArr[j];
                                j++;
			}
			k++;
		}

		while(i < n1){
		//while(i < leftArr.length){
		
			arr[k] = leftArr[i];
			k++;
			i++;
		}	
		 while(j < n2){
		 //while(j < rightArr.length){

                        arr[k] = rightArr[j];
                        k++;
                        j++;
                }

	}

	public static void main(String[] s){
	
		int arr[] = new int[]{9,7,5,3,4,6,8};

		divide(arr, 0, arr.length-1);

		for(int i = 0 ; i < arr.length ; i++){
		
			System.out.print(arr[i] + " ");
		}

		System.out.println();

	}
}
