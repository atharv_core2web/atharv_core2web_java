import java.util.*;

class quickSort{

	static void swap(int arr[] , int i , int j){
	
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}

	static int partition(int arr[] , int start, int end){
	
		int pivot = arr[end];
		int i = start - 1;
		for(int j = start ; j < end ; j++){
		
			if(arr[j] <= pivot){
			
				i++;
				swap(arr,i,j);
			}
		}

		swap(arr,i + 1,end);

		return i+1;
	}

	static void quickSort(int arr[],int start , int end){
		
		if(start < end){
		
			int pivotIdx = partition(arr,start,end);

			quickSort(arr, start, pivotIdx - 1);
			quickSort(arr, pivotIdx + 1, end);
		}
	
	}

	public static void main(String[] s){
	
		int arr[] = new int[]{12,7,6,14,5,15,10};

		quickSort(arr,0,arr.length - 1);

		System.out.println(Arrays.toString(arr));

	}


}
