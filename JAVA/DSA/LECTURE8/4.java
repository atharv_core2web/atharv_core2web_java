//BRUTEFORCE TC : O(N^2)
/*
class alphapairCnt{

	public static void main(String [] s){

		//char arr[] = new char[]{'a','b','e','g','a','g'};
		char arr[] = new char[]{'b','a','a','a','c','g','g','g'};
		
		int cnt = 0;

		for(int i = 0 ; i < arr.length ; i++){
			if(arr[i] == 'a'){
				for(int j = (i + 1) ; j < arr.length ; j++){
					if(arr[j] == 'g') {
					cnt++;
					}
				}
			}
		}
		System.out.println("Count : " + cnt);
		
	}
}*/

//OPTIMIZED TC : O(N)

class alphapairCnt{

        public static void main(String [] s){

                char arr[] = new char[]{'a','b','e','g','a','g'};
                //char arr[] = new char[]{'b','a','a','a','c','g','g','g'};

                int cntA = 0;

		int pair = 0;

                for(int i = 0 ; i < arr.length ; i++){
                        if(arr[i] == 'a'){
                        	cntA++;
			}	
			else if(arr[i] == 'g'){
                                pair += cntA;
			}
		}
                System.out.println("Count : " + pair);

        }
}
