//BRUTEFORCE TC : O(N^2)
/*class rightMax{

	public static void main(String[] s){

		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};

		int rightMax[] = new int[arr.length];

		int max = Integer.MIN_VALUE;

		for(int i = arr.length-1 ; i >= 0  ; i--){
			for(int j = i ; j >= 0 ; j--){
				if(max < arr[i]){
					max = arr[i];
				}

			rightMax[i] = max;
			}
		}

		for(int i = 0 ; i < rightMax.length ; i++){
			System.out.println(rightMax[i]);
		}
	}
}*/

//BRUTEFORCE TC : O(N)
class rightMax{

        public static void main(String[] s){

                int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};

                int rightMax[] = new int[arr.length];

                int max = Integer.MIN_VALUE;

		int N = arr.length;

                rightMax[N - 1] = arr[N - 1];

		for(int i = (N - 2) ; i >= 0 ; i--){
                       	if(rightMax[i + 1] < arr[i]){
				rightMax[i] = arr [i];
			}
			else{
				rightMax[i] = rightMax[i + 1];

                        }
                }

                for(int i = 0 ; i < rightMax.length ; i++){
                        System.out.println(rightMax[i]);
                }
        }
}
