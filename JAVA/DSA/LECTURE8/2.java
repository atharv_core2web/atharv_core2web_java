//BRUTEFORCE TC : O(N^2)
/*class leftMax{

	public static void main(String[] s){

		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};

		int leftMax[] = new int[arr.length];

		int max = Integer.MIN_VALUE;

		for(int i = 0 ; i < arr.length ; i++){
			for(int j = 0 ; j <= i ; j++){
				if(max < arr[i]){
					max = arr[i];
				}

			leftMax[i] = max;
			}
		}

		for(int i = 0 ; i < leftMax.length ; i++){
			System.out.println(leftMax[i]);
		}
	}
}*/

//BRUTEFORCE TC : O(N)
class leftMax{

        public static void main(String[] s){

                int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};

                int leftMax[] = new int[arr.length];

                int max = Integer.MIN_VALUE;

                leftMax[0] = arr[0];

		for(int i = 1 ; i < arr.length ; i++){
                       	if(leftMax[i - 1] < arr[i]){
				leftMax[i] = arr [i];
			}
			else{
				leftMax[i] = leftMax[i - 1];

                        }
                }

                for(int i = 0 ; i < leftMax.length ; i++){
                        System.out.println(leftMax[i]);
                }
        }
}
