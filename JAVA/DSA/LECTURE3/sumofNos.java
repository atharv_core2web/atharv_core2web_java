import java.util.*;


//BRUTEFORCE
class sumofNos{
	
	static int sum(int num){
	
		int sum = 0;
		for(int i = 1 ; i <= num ; i++){
			sum += i;
		}
		return sum;
	}	
	public static void main(String [] s){
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number till which sum is to be found : ");
		int num = sc.nextInt();
		int ret = sum(num);
		System.out.println("The sum of Numbers till " + num + " is: " + ret );
	}
}


//TIME COMPLEXITY : O(1)

/*class sumofNos{

        static int sum(int num){

                int sum = 0;
                sum = num*((num + 1))/2;
                return sum;
        }
        public static void main(String [] s){

                Scanner sc = new Scanner(System.in);
                System.out.println("Enter number till which sum is to be found : ");
                int num = sc.nextInt();
                int ret = sum(num);
                System.out.println("The sum of Numbers till " + num + " is: " + ret );
        }
}*/
