import java.util.*;

//BRUTEFORCE
class exclusiveDiff{

    static int difference(int start,int end){

	int cnt = 0;
        for(int i = start ; i <= end ; i++){
	
		cnt++;
	}
        return cnt;

    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter Starting of Range");
        int start = sc.nextInt();

        System.out.println("Enter Ending of Range");
        int end = sc.nextInt();

        int ans = difference(start,end);
        System.out.println("Difference from " + start + " to " + end + " is " + ans);

    }
}


//TIME COMPLEXITY : O(1)

/*class exclusiveDiff{

    static int difference(int start,int end){

        int diff = ((end - start  ) + 1);
        return diff;

    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Enter Starting of Range");
        int start = sc.nextInt();

        System.out.println("Enter Ending of Range");
        int end = sc.nextInt();

        int ans = difference(start,end);
        System.out.println("Difference from " + start + " to " + end + " is " + ans);

    }
}*/
