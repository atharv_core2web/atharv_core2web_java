//Q .3

class rangesumQuery{

	public static void main(String[] s){

		//int arr[] = new int[]{1,2,3,4,5};

		//int arr2[][] = new int[][]{{0,3} , {1,2}};
		
		int arr[] = new int[]{2,2,2};

		int arr2[][] = new int[][]{{0,0} , {1,2}};
		
		int N = arr.length;

		int prefixSum[] = new int[N];
		prefixSum[0] = arr[0];

		int ansArr[] = new int[arr2.length];

		int start = -1;
		int end = -1;
		
		int sum = 0;

		for(int i = 1 ; i < N ; i++){

			prefixSum[i] = prefixSum[i - 1] + arr[i];
		}

		for(int i = 0 ; i < N ; i++){
			for(int j = 0 ; j < arr2.length ; j++){
			
				start = arr2[j][0];
				end = arr2[j][1];
			
				if(start == 0)
					sum = prefixSum[end];
				else
					sum = prefixSum[end] - prefixSum[start - 1];
			
				ansArr[j] = sum;
			}
		}

		for(int i = 0 ; i < ansArr.length ; i ++){
			System.out.println(ansArr[i]);
		}

		//System.out.println(start + "  " + end);
	}
}

		
