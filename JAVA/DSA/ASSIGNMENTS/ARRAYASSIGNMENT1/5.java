//Q .5

class inplaceprefixSum{

	public static void main(String[] s){
	
		int arr[] = new int[]{4,3,2};
		//int arr[] = new int[]{1,2,3,4,5};

		int N = arr.length;

		for(int i = 1 ; i < N ; i++){
			arr[i] += arr[i - 1];
		}
		
		for(int i = 0 ; i < N ; i++){
			System.out.print(arr[i] + " ");
		}
	}
}
