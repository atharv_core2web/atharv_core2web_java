//Q .4
class timeEquality {

	public static void main(String[] s){

		int arr[] = new int[]{2,4,1,3,2};

		int N = arr.length;

		int max = Integer.MIN_VALUE;

		int diff = 0;

		int sum = 0;
		
		for(int i = 0 ; i < N ; i++){

			if(arr[i] > max)
				max = arr[i];
		}

		for(int i = 0 ; i < N ; i++){
		 
			diff = max - arr[i];
			sum += diff;
		}

		System.out.println(sum);

	}
}
