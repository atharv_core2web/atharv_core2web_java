//Q .1
class minmaxSum {

	public static void main(String[] s){

		int arr[] = new int[]{1,3,4,1};
		//int arr[] = new int[]{-2,1,-4,5,3};

		int N = arr.length;

		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
		
		for(int i = 0 ; i < N ; i++){

			if(arr[i] > max)
				max = arr[i];
			if(arr[i] < min)
				min = arr[i];
		}

		System.out.println(max + min);

	}
}
