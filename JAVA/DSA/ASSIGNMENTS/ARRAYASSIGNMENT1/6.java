//Q.6

class productArrayPuzzle{

	public static void main(String[] s){

		int arr[] = new int[]{5,1,10,1};
		//int arr[] = new int[]{1,2,3,4,5};

		int N = arr.length;

		int product = 1;
		
		int ansArr[] = new int[N];

		for(int i = 0 ; i < N ; i++){
			product = 1;
			for(int j = 0 ; j < N ; j++){
				if(i != j)
					product *= arr[j];
			}
			ansArr[i] = product;
		}
	
		for(int i = 0 ; i < N ; i++){
			System.out.print(ansArr[i] + " ");
		}
			
	}
}
