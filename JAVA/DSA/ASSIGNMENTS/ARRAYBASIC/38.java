class immediateSmaller{

	public static void main(String[] s){

		int arr[] = new int[]{5,6,2,3,1,7};
		//int arr[] = new int[]{4,2,1,5,3};

		int N = arr.length;
		
		int current_index = Integer.MAX_VALUE;

		for(int i = 0 ; i < N ; i++){

			if(arr[i] == arr[N - 1] || arr[i] < arr[i + 1]){
				System.out.println("-1 ");
			}
			else if(arr[i] > arr[i + 1]){
				current_index = (arr[i + 1]);
				System.out.println(current_index + " ");
			}	
			else{
				current_index = arr[i];
				//current_index = (i);
				System.out.println(current_index + " ");
			}
		}	
	}
}

