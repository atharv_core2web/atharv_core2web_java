class moveallNegatives{

	public static void main(String[] s){

		int arr[] = new int[]{-12,11,-13,-5,6,-7,5,-3,-6};

		int N = arr.length;
		
		int temp = 0;
		
		int j = 0;
		for(int i = 0 ; i < (N ) ; i++){
                      
			if(arr[i] < 0){ 
				if((i != j)){
                                	temp = arr[i];
                                	arr[i] = arr[j];
                                	arr[j] = temp;
                        	}
				j++;
                	        
			}
		}

		for(int i = 0 ; i < N ; i++){
			
			System.out.print(arr[i] + " ");	
		}
	}

}
