//BRUTEFORCE TC : 0(N^2)

/*class greaterThan{

	public static void main(String[] s){

		int arr[] = new int[]{2,5,1,4,8,0,8,1,3,8};

		int N = 10;

		int cnt = 0;

		for(int i = 0 ; i < N ; i++){
			for(int j = 0 ; j < N ; j++){
				if(arr[i] < arr[j]){
					cnt++;
					break;
				}
			}
		}
		
		System.out.println(cnt);	
	}
}
*/


//OPTIMIZED TC : O(N)

class greaterThan{

        public static void main(String[] s){

                int arr[] = new int[]{2,5,1,4,8,0,8,1,3,8};

                int N = 10;

                int cnt = 0;
	
		int max = Integer.MIN_VALUE;
                for(int i = 0 ; i < N ; i++){
                        if(max < arr[i]){
				max = arr[i];
			}
		}

	 	for(int i = 0 ; i < N ; i++){
                        if(max == arr[i]){
                                cnt++;
                        }
                }

                System.out.println(N-cnt);
        }
}
