//BRUTEFORCE TC : O(N^2)  

class pairCnt{

	public static void main(String[] s){

		int arr[] = new int[]{3,5,2,1,-3,7,8,15,6,13};

		int N = 10;

		int K = 10;

		int cnt = 0;

		for(int i = 0 ; i < N ; i++){
			//for(int j = 0 ; j < N ; j++){
			//ONLY ITERATIONS DECREASE BUT TC DOESN'T AS ONLY HALF ARRAY IS SEARCHED ie. (i,j) == (j,i) pair is NOT checked twice
			
			for(int j = i+1 ; j < N ; j++){
				if((arr[i] + arr[j] == K) && (i != j)){
					cnt++;
				}
			}
		}
		
		//System.out.println(cnt);
		System.out.println(cnt * 2);
	}
}


