class ArrayDemo{

	public static void main(String[] s){
		int arr[] = new int[]{5,6,2,3,1,9};
		
		int N = 6;

		for(int i = 0 ; i < N ; i++){
			System.out.println(arr[i]);
		}
	}
}

//TIME COMPLEXITY : O(N)
//SPACE COMPLEXITY : O(1)
