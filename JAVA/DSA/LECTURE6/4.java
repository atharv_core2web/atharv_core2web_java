//WRITE A CODE TO REVERSE AN ARRAY WITH TC O(N)  
/*
class revArr{

	public static void main(String[] s){

		int arr[] = new int[]{8,4,1,3,9,2,6,7};
			
		int N = 8;

		for(int i = 0 ; i < (N / 2) ; i++){
			int temp = arr [i];
			arr[i] = arr [N - i - 1];
			arr [N - i - 1] = temp;
		}

		
		for(int i = 0 ; i < N  ; i++){

			System.out.println(arr[i]);
                }
	}
}*/

//WRITE A CODE TO REVERSE AN ARRAY WITH TC O(1)

class revArr{

        public static void main(String[] s){

                int arr[] = new int[]{8,4,1,3,9,2,6,7};

                int N = 8;

		int i = 0;

		int j = N - 1;

		while(i < j){
			int temp = arr[i];
			arr[i] = arr[j];
			arr[j] = temp;

			i++;
			j--;
		}


		 for(int k = 0 ; k < N  ; k++){

                        System.out.println(arr[k]);
                }
	}
}


