//BRUTEFORCE TC : O(N^2)
/*
class EqSum{

	public static void main(String[] s){

		//int arr[] = new int[]{-7,1,5,2,-4,3,0};
		//int arr[] = new int[]{0,1,3,-2,-1};
		int arr[] = new int[]{1,3,5,2,2};

		int N = arr.length;

		int flag = 0;
		
		for(int i = 0 ; i < N ; i++){
			int rightSum = 0;
			int leftSum = 0;
				for(int j = 0 ; j < i ; j++){
			       			leftSum += arr[j];
				}	
	 		
				for(int k = i + 1 ; k < N ; k++){
                               			rightSum += arr[k];
                        		
				}	

				if(rightSum == leftSum){
					flag = 1;
					System.out.println(i);
					break;
				}
		}
		
		if(flag == 0){			
			System.out.println("-1");
		
		}			
		
	}
}*/

//OPTIMIZED(PREFIX SUM) TC : O(N) SC : O(N)


class EqSum{

        public static void main(String[] s){

                int arr[] = new int[]{-7,1,5,2,-4,3,0};
                //int arr[] = new int[]{0,1,3,-2,-1};
                //int arr[] = new int[]{1,3,5,2,2};

                int N = arr.length;

		int prefixSum[] = new int[N];
                prefixSum[0] = arr[0];

		int flag = 0;

		
                for(int i = 1 ; i < N ; i++){
			prefixSum[i] = prefixSum[i - 1] + arr[i];
		}
		
		int total = prefixSum[N - 1];
		for(int i = 1 ; i < N ; i++){
                        int rightSum = 0;
                        int leftSum = 0;
			
			//int total = prefixSum[N - 1];
			leftSum = prefixSum[i - 1];
			rightSum = total - prefixSum[i];
			
			if(leftSum == rightSum){
				flag = 1;
				System.out.println(i);
				break;
			}

		
		}

		if(flag == 0){			
			System.out.println("-1");
		}

	}
}
