import java.util.*;

class Node{

	int data;
	Node next = null;
	Node prev = null;

	Node(int data){
	
		this.data = data;
	}
}

class DoublyLinkedList{

	Node head = null;
	
	//SCENARIO 1 : DLL EMPTY
	//SCENARIO 2 : DLL EMPTY
	//SCENARIO 1 : DLL EMPTY
	void addFirst(int data){
	
		Node newNode = new Node(data);
		if(head == null){
		
			head = newNode;
		}else{
		
			newNode.next = head;
			head.prev = newNode;
			head = newNode;
		}


	}
	
	void addLast(int data){
	
		Node newNode = new Node(data);
		Node temp = head;

		if(head == null){
			
			head = newNode;
		}else{
		
			while(temp.next != null){
			
				temp = temp.next;
			}

			newNode.prev = temp;
			temp.next = newNode;
		
		}
	}
	
	void addAtPos(int pos,int data){
	
		if(pos <= 0 || pos >= countNode() + 2){
		
			System.out.println("Wrong Input");
		}
		
		Node newNode = new Node(data);

		if(pos == 1)
			addFirst(data);
		else if(pos == countNode() + 1)
			addLast(data);
		else{
			Node temp = head;
			while((pos - 2) != 0){
		
				temp = temp.next;
				pos--;
			}
		
			newNode.prev = temp;
                	newNode.next = temp.next;
                	newNode.next.prev = newNode;
			temp.next = newNode;
		}
	}
	
	void deleteFirst(){
	
		if(head == null)
			System.out.println("LL is Empty");
		else if(countNode() == 1)
			head = null;
		else{
		
			head = head.next;
			head.prev = null;
		}
	}
	
	void deleteLast(){
		if(head == null)
                        System.out.println("LL is Empty");
                else if(countNode() == 1)
                        head = null;
                else{
			Node temp = head;
                        
			while(temp.next != null){
				temp = temp.next;
			}

			//GC ONLY CHECK INCOMING REFERENCES`
                        temp.prev.next = null;
                	temp.prev = null;
		}
	}
	
	void deleteAtPos(int pos){}
	
	int countNode(){
	
		if(head == null)
			return 0;
		
		int cnt = 0;
		Node temp = head;

		while(temp != null){
		
			cnt++;
			temp = temp.next;
		}
			
		return cnt;
	}
	

	//SCENARIO 1 : DLL IS EMPTY
	//SCENARIO 2 : DLL IS NOT EMPTY
	void printDLL(){
	
		Node temp = head;
		if(head == null){
		
			System.out.println("DLL is empty");
		}else{
		
			while(temp != null){
			
				if(temp.next != null)
					System.out.print(temp.data + " -> ");
				else
					System.out.print(temp.data);
				
				temp = temp.next;
			}
		
		}
		System.out.println();
	}
}


class Client{

        public static void main(String[]s){

                DoublyLinkedList ll = new DoublyLinkedList();    
            	char ch;
                do{

                        System.out.println("SINGLE LINKED LIST");
                        System.out.println("****** MENU ******");
                        System.out.println("1. ADD FIRST");
                        System.out.println("2. ADD LAST");
                        System.out.println("3. ADD AT POSTION");
                        System.out.println("4. DELETE FIRST");
                        System.out.println("5. DELETE LAST");
                        System.out.println("6. DELETE AT POSITION");
                        System.out.println("7. COUNT NODES");
                        System.out.println("8. PRINT LIST");
                        System.out.println("9. EXIT");


                Scanner sc = new Scanner(System.in);
                int choice = sc.nextInt();

                switch(choice){
                        case 1: {
                                System.out.println("Enter Data : ");
                                int data = sc.nextInt();
                                ll.addFirst(data);
                        }
                                break;

                        case 2: {
                                System.out.println("Enter Data : ");
                                int data = sc.nextInt();
                                ll.addLast(data);
                        }
                                break;

                        case 3: {
                                System.out.println("Enter Data : ");
                                int data = sc.nextInt();
                                System.out.println("Enter Position : ");
                                int pos = sc.nextInt();
                                ll.addAtPos(pos,data);
                        }
                                break;

                        case 4 : {

                                ll.deleteFirst();

                        }
                                break;

                        case 5 : {

                                ll.deleteLast();
                        }
                                break;

                        case 6 : {

                                System.out.println("Enter Position : ");
                                int pos = sc.nextInt();
                                ll.deleteAtPos(pos);
                        }
                                break;

                        case 7 : {

                                int nodes = ll.countNode();
                                System.out.println("Number of Nodes in Linked List : " + nodes);
                        }
                                break;
                        case 8 : {

                                System.out.println("Linked List : ");
                                ll.printDLL();
                        }
                                break;
                        case 9 : {

                                System.exit(0);
                        }
                                break;
                        default :
                                System.out.println("Wrong Input : ");
                                break;
                }


                        System.out.println("Do You Want to Continue ?");
                        ch = sc.next().charAt(0);
                }while(ch == 'Y' || ch == 'y');
        }
}
