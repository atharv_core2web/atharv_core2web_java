class rotate90deg{

        public static void main(String[] s){

                int arr[][] = new int[][]{{1,2,3,4,5} , {6,7,8,9,10} , {11,12,13,14,15} , {16,17,18,19,20} , {21,22,23,24,25 }};
		//int arr[][] = new int[][]{{1,2,3,4} , {5,6,7,8} , {9,10,11,12} , {13,14,15,16}};
                //int arr[][] = new int[][]{{1,2,3,4,5} , {6,7,8,9,10} , {11,12,13,14,15} , {16,17,18,19,20} , {21,22,23,24,25 }, {26,27,18,29,30}};

		int N = arr.length;
		

		//TRANSPOSE
		for(int i = 0 ; i < (N ) ; i++){
			for(int j = (i + 1) ; j < (N ) ; j++){
				int temp = 0;
				temp = arr[i][j];
				arr[i][j] = arr[j][i];
				arr[j][i] = temp;
			}
		}	

		//COLUMN SWAP
		for(int i = 0 ; i < (N) ; i++){
                        	int start = 0;
				int end = N - 1;
				
				while(start < end){
					int temp = 0;
					temp = arr[i][start];
                                	arr[i][start] = arr[i][end];
                                	arr[i][end] = temp;
					
					start++;
					end--;
				}
				
                        }
		

		for(int i = 0 ; i < (N) ; i++){
                        for(int j = 0 ; j < (N) ; j++){

                                System.out.print(arr[i][j] + "    ");
                        }
                        System.out.println();
                }
	}
}
