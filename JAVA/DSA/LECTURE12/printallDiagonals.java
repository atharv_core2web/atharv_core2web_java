class printallDiagonal {

        public static void main(String[] s){

                int arr[][] = new int[][]{{1,2,3,4,5,6} , {7,8,9,10,11,12} , {13,14,15,16,17,18} , {19,20,21,22,23,24} , {25,26,27,28,29,30}};

                int M = arr[0].length;
                int N = arr.length;

		//LEFT DIAGONALS
		System.out.println("LEFT DIAGONALS : ");
		for(int j = (M - 1) ; j >= 0 ; j--){

			int i = 0;
			int y = j;

			while(i < N && y >= 0){

				System.out.print(arr[i][y] + "	");
				i++;
				y--;
			}	
		System.out.println();
		}

		System.out.println("RIGHT DIAGONALS : ");
		for(int j = 0 ; j <= (M - 1) ; j++){
			
			int i = 0;
			int y = j;
			
			while(i < N && y <= (M - 1)){
				System.out.print(arr[i][y] + "	");
                                i++;
                                y++;
			}
		System.out.println();	
		}

		
	}
}
