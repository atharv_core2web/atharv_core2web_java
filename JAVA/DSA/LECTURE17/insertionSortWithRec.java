class insertionSortWithRec{

	static void sort(int arr[],int start){
	
		if(start == arr.length - 1)
			return;

		int i = start + 1;
		int j = i - 1;
		int element = arr[i];
		int n = arr.length;
			
			//while(arr[j] > element && j >= 0){  // FOR THIS CONDITION IN CASE OF J = -1 ARR[-1] THROWS INDEXOUTPFBOUNDS

			while(j >= 0 && arr[j] > element){
			
				arr[j + 1] = arr[j];
				j--;
			}
			arr[j + 1] = element;

		sort(arr,start + 1);
	}
	

	public static void main(String[] s){
	
		int arr[] = new int[]{2,14,0,9,5,7};

		sort(arr,0);

		for(int i = 0 ; i < arr.length ; i++){
		
			System.out.print(arr[i] + " ");
		}
	}
}
