class bubbleSortwithRecursion{

	static int cnt = 0;
	static void sort(int arr[],int n){
		boolean swapped = false;

		if(n == 1){
			return;
		}	

		for(int j = 0 ; j < n - 1 ; j++){
                    cnt++;
			if(arr[j] > arr[j + 1]){                                                                                                                                             int temp = arr[j];                                                                                                                                          arr[j] = arr[j + 1];                                                                                                                                        arr[j + 1] = temp;
		    	    swapped = true;	
		    }	
		}

		//sort(arr , n - 1);
	
		if(swapped == false)
			return;
		sort(arr , n - 1);
		
	}	
	public static void main(String[] s){
                int arr[] = new int[]{2 ,3 ,4 ,5 ,6 ,7,9};
                //int arr[] = new int[]{3 , 7 ,9 ,4 ,2 ,5 ,6};
		int n = arr.length;
                sort(arr , n);
        	for(int i = 0 ; i < n ; i++){
			System.out.print(arr[i] + " ");
		}
		System.out.println("cnt : " + cnt);
	}
}
