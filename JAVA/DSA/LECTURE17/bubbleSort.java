class bubbleSort{
/*
	static void sort(int arr[]){
	
		int n = arr.length;
		int cnt = 0;

		//THIS CODE GIVES SAME NO. OF  ITERATIONS EVEN IF ARRAY IS ALREADY SORTED
		for(int i = 0 ; i < n - 1 ; i++){
			for(int j = 0 ; j < n - i - 1 ; j++){
				cnt++;
				if(arr[j] > arr[j+1]){
					int temp = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = temp;
				}	
			}
			
		}
		
		for(int i = 0 ; i < n ; i++){
			System.out.print(arr[i] + " ");
		}
			System.out.println(cnt + " ");
	}

*/

	//DRASTICALLY DECREASES ITERATION
	static void sort(int arr[]){
		int n = arr.length;
                int cnt = 0;
		boolean swapped = false;
                for(int i = 0 ; i < n - 1 ; i++){
                        for(int j = 0 ; j < n - i - 1 ; j++){
                                cnt++;
                                if(arr[j] > arr[j+1]){
                                        int temp = arr[j];
                                        arr[j] = arr[j + 1];
                                        arr[j + 1] = temp;
                                	swapped = true;
				}
                        }
			if(swapped == false)
				break;
			

                }

                for(int i = 0 ; i < n ; i++){
                        System.out.print(arr[i] + " ");
                }
                        System.out.println(cnt + " ");
        	
	}
	public static void main(String[] s){
		int arr[] = new int[]{2 ,3 ,4 ,5 ,6 ,7,9};
		//int arr[] = new int[]{3 , 7 ,9 ,4 ,2 ,5 ,6};

		sort(arr);
	}
}
