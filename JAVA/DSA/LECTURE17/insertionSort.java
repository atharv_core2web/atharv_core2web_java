class insertionSort{

	static void sort(int arr[]){
	
		int n = arr.length;

		for(int +i = 1 ; i < n ; i++){
			int element = arr[i];
			int j = i - 1;
			
			//while(arr[j] > element && j >= 0){  // FOR THIS CONDITION IN CASE OF J = -1 ARR[-1] THROWS INDEXOUTPFBOUNDS

			while(j >= 0 && arr[j] > element){
			
				arr[j + 1] = arr[j];
				j--;
			}
			arr[j + 1] = element;

		}
	}

	public static void main(String[] s){
	
		int arr[] = new int[]{2,14,0,9,5,7};

		sort(arr);

		for(int i = 0 ; i < arr.length ; i++){
		
			System.out.print(arr[i] + " ");
		}
	}
}
