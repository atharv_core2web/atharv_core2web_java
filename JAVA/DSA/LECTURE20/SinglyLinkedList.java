import java.util.*;

class Node{

	int data;
	Node next = null;

	Node(int data){
	
		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	//ADDING DATA TO START OF LINKED LIST
	//SCENARIO 1 : LL EMPTY
	void addFirst(int data){
	
		Node newNode = new Node(data);

		if(head == null){
		
			head = newNode;
		}else{
		
			newNode.next = head;
			head = newNode;
		}
	}

	//ADDING DATA TO END OF LINKED LIST
	//SCENARIO 1 : LL EMPTY
	void addLast(int data){
	
		Node newNode = new Node(data);

		if(head == null){
		
			head = newNode;
		}else{
		
			Node temp = head;

			while(temp.next != null){
			
				temp = temp.next;
			}

			temp.next = newNode;
		}
	}

	//COUNTS NUMBER OF NODES OF LINKED LIST
	int countNode(){
	
		Node temp = head;
		int cnt = 0;

		while(temp != null){
		
			cnt++;
			temp = temp.next;
		}

		return cnt;
	}

	//ADDS DATA AT PATICULAR POSITION
	
	//SCENARIO 1 : POSITION 0 OR NEGATIVE OR GREATER THAN TOTAL NODES
	//SCENARIO 2 : POSTION FIRST OR LAST
	//SCENARIO 3 : POSTION SOMEWHERE BETWEEN NODES ALREADY PRESENT 
	void addAtPos(int pos,int data){
	
		if(pos <= 0 || pos >= countNode() + 2){
		
			System.out.println("Wrong Postion");
			return;
		}else if(pos == 1){
		
			addFirst(data);
		}else if(pos == countNode() + 1){
		
			addLast(data);
		}else{
			
			Node newNode = new Node(data);
			Node temp = head;

			// -1 TO STOP BEFORE POSTION
			// -1 AS TEMP IS ALREADY POINTING TO HEAD
			// THEREFOR -1-1 = -2 i.e (pos -2)
			while((pos-2) != 0){
		
				temp = temp.next;
				pos--;
			}

			newNode.next = temp.next;
			temp.next = newNode;
		}
	}

	//PRINTING LINKED LIST
	void printSLL(){
	
		if(head == null)
			System.out.println("Empty Linked List");
		else{
		
			Node temp = head;
			while(temp != null){
			
				//THIS IF ELSE IS JUST TO HANDLE ARROW(FORMATTING)
				if(temp.next != null){
					System.out.print(temp.data + "->");
					temp = temp.next;
				}else{
					System.out.print(temp.data);
                                	temp = temp.next;
				}
				


			}
		}
		System.out.println();
	}

	//DELETED FIRST ELEMENT
	//SCENARIO 1 : LL EMPTY
	//SCENARIO 2 : IF ONLY 1 NODE
	//SCENARIO 3 : IF MANY NODES
	void deleteFirst(){
	
		if(head == null){
		
			System.out.println("Linked List is Empty");
			return;
		}
		if(countNode() == 1){
			head = null;
			return;
		}
		else{
		
			//JUST IGNORE FIRST NODE SO THAT GC CLEARS IT
			head = head.next;
		}

	}

	//DELETES LAST NODE
	//SCENARIO 1 : LL EMPTY
        //SCENARIO 2 : IF ONLY 1 NODE
        //SCENARIO 3 : IF MANY NODES
	void deleteLast(){
	
		if(head == null){
			System.out.println("Linked List is Empty");
			return;
		}
		if(countNode() == 1){
			head = null;
			return;
		}

		Node temp = head;
		while(temp.next.next != null){
		
			temp = temp.next;

		}	
		temp.next = null;
	}

	//SCENARIO 1 : POSITION 0 OR NEGATIVE OR GREATER THAN TOTAL NODES
        //SCENARIO 2 : POSTION FIRST OR LAST
        //SCENARIO 3 : POSTION SOMEWHERE BETWEEN NODES ALREADY PRESENT
        void deleteAtPos(int pos){

                if(pos <= 0 || pos >= countNode() + 1){

                        System.out.println("Wrong Postion");
                        return;
                }else if(pos == 1){

                        deleteFirst();
                }else if(pos == countNode() + 1){

                        deleteLast();
                }else{

                        Node temp = head;

                        // -1 TO STOP BEFORE POSTION
                        // -1 AS TEMP IS ALREADY POINTING TO HEAD
                        // THEREFOR -1-1 = -2 i.e (pos -2)
                        while((pos-2) != 0){

                                temp = temp.next;
                                pos--;
                        }

                    
                        temp.next = temp.next.next;
                }
        }
}


class Client{

	public static void main(String[]s){
	
		LinkedList ll = new LinkedList();
/*
		System.out.println("After add First");
		ll.addFirst(10);
		ll.addFirst(20);
		ll.addFirst(30);
		ll.printSLL();
		

		System.out.println("After add Last");
		ll.addLast(15);
		ll.printSLL();
			
		System.out.println("After add at pos");
		ll.addAtPos(5,25);
		ll.printSLL();

		int nodes = ll.countNode();
		System.out.println("Number of Nodes in Linked List : " + nodes);
		ll.printSLL();
	
		System.out.println("After Delete First");
		ll.deleteFirst();
		ll.printSLL();
		
		System.out.println("After Delete Last");
		ll.deleteLast();
		ll.printSLL();
		
		
		ll.addFirst(100);
		ll.addFirst(200);
		ll.addFirst(300);
		System.out.println("After del at pos");
		ll.deleteAtPos(3);
		ll.printSLL();
	*/
		char ch;
		do{
		
			System.out.println("SINGLE LINKED LIST");
			System.out.println("****** MENU ******");
			System.out.println("1. ADD FIRST");
			System.out.println("2. ADD LAST");
			System.out.println("3. ADD AT POSTION");
			System.out.println("4. DELETE FIRST");
			System.out.println("5. DELETE LAST");
			System.out.println("6. DELETE AT POSITION");
			System.out.println("7. COUNT NODES");
			System.out.println("8. PRINT LIST");
			System.out.println("9. EXIT");
			
			
		Scanner sc = new Scanner(System.in);
		int choice = sc.nextInt();
		
		switch(choice){
			case 1: {
				System.out.println("Enter Data : ");
				int data = sc.nextInt();
				ll.addFirst(data);	
			}
				break;
			
			case 2: {
                                System.out.println("Enter Data : ");
                                int data = sc.nextInt();
                                ll.addLast(data);
			}
			   	break;
                        
			case 3: {
                                System.out.println("Enter Data : ");
                                int data = sc.nextInt();
                                System.out.println("Enter Position : ");
                                int pos = sc.nextInt();
                                ll.addAtPos(pos,data);
			}
				break;
                        
			case 4 : {
			
				ll.deleteFirst();
			
			}
			 	break;
			
			case 5 : {

                                ll.deleteLast();
			}
				break;
                        
			case 6 : { 
			
				System.out.println("Enter Position : ");
                               	int pos = sc.nextInt();
                                ll.deleteAtPos(pos);
			}
				break;
			
			case 7 : {
			
				int nodes = ll.countNode();
                		System.out.println("Number of Nodes in Linked List : " + nodes);
			}
				break;
			case 8 : {
			
				System.out.println("Linked List : ");
				ll.printSLL();
			}
				break;
			case 9 : {

                                System.exit(0);
                        }
                                break;
			default : 
				System.out.println("Wrong Input : ");
				break;
		}
	
		
			System.out.println("Do You Want to Continue ?");
			ch = sc.next().charAt(0);
	        }while(ch == 'Y' || ch == 'y');
	}
}

