class cntsubArr{

	public static void main(String[] s){

		int arr[] = new int[]{3,12,-2,15};
		//int arr[] = new int[]{4,2,10,3,12,-2,15};

		int N = arr.length;

		int cnt = 0;

		for(int i =  0 ; i < N ; i++){
			for(int j = i ; j < N ; j++){
				cnt++;
			}
		}

		System.out.println(cnt);
	}
}
//TOTAL SUBARRAYS OF AN ARRAY CAN BE CALCULATED BY GAUSS THEOREM
//  N * (N + 1) / 2
