class printtotalsumofsubArr{

        public static void main(String[] s){

                int arr[] = new int[]{1,2,3};
                //int arr[] = new int[]{2,4,1,3};

                int N = arr.length;

		int total  = 0;
                for(int i = 0 ; i < N ; i++){
                        int sum = 0;
                        for(int j = i ; j < N ; j++){
                                sum = sum += arr[j];
                               	total += sum;
			       	System.out.println("Sum :" + sum);
                        }
		}
		System.out.println("Total : " + total);	
        }
}
