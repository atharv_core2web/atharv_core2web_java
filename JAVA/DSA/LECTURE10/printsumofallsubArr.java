//BRUTEFORCE 
//TC : O(N^3) SC = O(1)

/*
class printumofsubArr{

	public static void main(String[] s){

		int arr[] = new int[]{2,4,1,3};
		
		int N = arr.length;

		for(int i = 0 ; i < N ; i++){
			//int sum = 0;
			for(int j = i ; j < N ; j++){
			int sum = 0;
				for(int k = i ; k <= j ; k++){
					sum += arr[k];
					System.out.print(arr[k]  + " " );
				//	System.out.print("Sum : "+ sum  + " " );
				}
 				System.out.print("Sum : "+ sum  + " " );	
				System.out.println();
			}
		}

	}
}*/


//USING PREFIX SUM 
//TC : O(N^2)  SC : O(N)

/*
class printumofsubArr{

        public static void main(String[] s){

                int arr[] = new int[]{2,4,1,3};

                int N = arr.length;

		int prefixSum[] = new int[N];
		prefixSum[0] = arr[0];

                for(int i = 1 ; i < N ; i++){
			prefixSum[i] = prefixSum[i - 1] + arr[i];
		}

                for(int i = 0 ; i < N ; i++){
                        for(int j = i ; j < N ; j++){
                        int sum = 0;
                        if(i == 0)
				sum = prefixSum[j];
			else
				sum = prefixSum[j] - prefixSum[i - 1];
			
			System.out.println("Sum : " + sum);
			}
		}
	}
}

*/

//CARRY FORWARD METHOD
//TC : 0(N^2)  SC : O(1)

class printumofsubArr{

        public static void main(String[] s){

                int arr[] = new int[]{2,4,1,3};

                int N = arr.length;

		for(int i = 0 ; i < N ; i++){
                        int sum = 0;
                        for(int j = i ; j < N ; j++){
                                sum = sum += arr[j];
				System.out.println("Sum :" + sum);
			}
		}
	}
}
