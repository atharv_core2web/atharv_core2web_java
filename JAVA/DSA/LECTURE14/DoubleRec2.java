class DoubleRec2{
	void fun(int num){
		if(num <= 1)
			return;

		fun(num - 2);
		System.out.println(num);
		fun(num - 1);
		
	}

	public static void main(String[] s){
		DoubleRec2 obj = new DoubleRec2();
		obj.fun(5);
	}
}
