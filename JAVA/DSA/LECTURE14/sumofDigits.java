class sumofDigits{
	//int sum = 0;
	
	int fun(int num){
		int digit = 0;
		
		//(REQUIRE GLOBAL SUM VARIABLE AND RETURN LINE OF FUNCTION))
		/*while(num > 0){

			digit = num % 10;
			sum += digit;
			num /= 10;
		}*/

		//(REQUIRE GLOBAL SUM VARIABLE AND RETURN LINE OF FUNCTION))
		/*for(int i = num ; i > 0 ; i /= 10){
			digit = i % 10;
                        sum += digit;
		}*/

		//USING RECURSION(REQUIRE GLOBAL SUM VARIABLE AND RETURN LINE OF FUNCTION))
		/*if(num == 0)
			return sum;

		sum += num % 10;
		fun(num / 10);*/

		//USING RECURSION(DONT REQUIRE GLOBAL SUM VARIABLE AND RETURN LINE OF FUNCTION)
		if(num == 0)
			return 0;

		return (num % 10) + fun(num / 10);


	//return sum;
	}

	public static void main(String[] s){

		sumofDigits obj = new sumofDigits();
		int ret = obj.fun(173);
		System.out.println(ret);
	}
}
