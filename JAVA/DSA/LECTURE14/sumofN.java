class sumofN{

	//USING FOR LOOP
	/*void fun(int num){

		int sum = 0;
		for(int i  = 1 ; i <= num ; i++){
			sum = sum + i;
		}	
		System.out.println(sum);
	}*/

	//APPROACH 1
	/*int fun(int num){

		if(num == 0)
			return 0;

		return num + fun(--num);
	}*/

	//APPROACH 2
	/*int fun(int num){

		if(num == 1)
			return 1;

		return num + fun(--num);
	}*/
	
	//APPROACH 3(USING SUM VARIABLE)
	int sum = 0;
	int fun(int num){

		if(num == 0)
			return 1;
		sum += num;

		fun(--num);

		return sum ;
	}

	public static void main(String[] s){

		System.out.println("Start Main");

		sumofN obj = new sumofN();
		//obj.fun(5);

		int ret = obj.fun(5);
		System.out.println(ret);
		System.out.println("End Main");
	}
}
