class Recursion3{

	        int fun(int num){
                if(num == 1)
                        return 1;

		return  fun(--num) + 3;

        	}

        	public static void main(String[] s){

                	System.out.println("Start Main");
                	Recursion3 obj = new Recursion3();
                	int ret = obj.fun(2);
			System.out.println(ret);
			System.out.println("End Main");
        	}
}
