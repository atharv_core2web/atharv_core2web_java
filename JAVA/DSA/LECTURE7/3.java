/*
 GIVE SUM OF GIVEN INTEGER ARRAY WITHOUT USING EXTRA ARRAY

CONSTRAINTS:

1 <= N <= 10^5
1 <= A[I] <= 10^3

WRITE SEPARATE FUNCTION FOR IT
*/

import java.util.*;

class prefixSum{

	static int[] preSum(int arr[]){
		
		for(int i = 1 ; i < arr.length ; i++){
			arr[i] = arr[i] + arr[i -1];
		}

		return arr;
	}

	public static void main(String[] s){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size of Array :");
		int N = sc.nextInt();

		int arr[] = new int[N];

		for(int i = 0 ; i < arr.length ; i++){
			System.out.print("Enter Element " + (i + 1 ) + " :");
			arr[i] = sc.nextInt();
		}

		int sum[] = preSum(arr);
		System.out.println("Prefix Sum Array : "); 
		for(int i = 0 ; i < sum.length ; i++){
                        System.out.println(sum[i]);
                }
	}
}

