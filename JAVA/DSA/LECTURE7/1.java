import java.util.*;

class suminRange{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		int arr[] = new int[]{2,5,3,11,7,9,4};

		System.out.print("Enter Starting Index of range : ");
		int start = sc.nextInt();
	
		System.out.print("Enter Ending Index of range : ");
		int end = sc.nextInt();

		int sum = 0;

		for(int i = start ; i <= end ; i++){
			sum += arr[i];
		}

		System.out.println("Sum : " + sum);
	}
}


