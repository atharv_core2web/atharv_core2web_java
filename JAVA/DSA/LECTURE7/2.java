import java.util.*;

//BRUTEFORCE 
//TC : O(Q * N) 
/*class suminRangeQuery{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};
		
		System.out.print("Enter Number of Queries : ");
		int Q = sc.nextInt();

		for(int j = 0 ; j < Q ; j++){
			System.out.print("Enter Starting Index of range : ");
			int start = sc.nextInt();
	
			System.out.print("Enter Ending Index of range : ");
			int end = sc.nextInt();

			int sum = 0;

			for(int i = start ; i <= end ; i++){
				sum += arr[i];
			}

			System.out.println("queries " +"  s  "  + " e "+ "  sum ");
			System.out.println("query "+ (j + 1) +":  " +start + "   " + end + "    "+ sum);
		}
	}
}*/

//PREFIX SUM
//TC : 0() SC : O(N)
class suminRangeQuery{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};

                int psArr[] = new int[arr.length];
		
		psArr[0] = arr[0];
		
		System.out.print("Enter Number of Queries : ");
                int Q = sc.nextInt();

		for(int i = 1 ; i < arr.length ; i++){
			psArr[i] = psArr[i - 1] + arr[i];
		}

		int sum = 0;

                for(int i = 0 ; i < Q ; i++){
                        System.out.print("Enter Starting Index of range : ");
                        int start = sc.nextInt();

                        System.out.print("Enter Ending Index of range : ");
                        int end = sc.nextInt();        
			
			//THIS APPROACH CREATES PROBLEM IF USER GIVES RANGE 0 TO 0 HENCE USE IF ELSE TO HANDLE IT\
			if(start == 0){
				sum = psArr[end];
			}
			else{
				sum = psArr[end] - psArr[start - 1];
			}

			System.out.println("Sum : " +sum);
		}
	}
}
