class fiboSeries{

	static int fibo(int num){
	
		if(num < 1)
			return num;

		return fibo(num - 1) + fibo(num - 2);
	}
	public static void main(String[] s){
	
		//WITHOUT RECURSION
		/*int a = 0;
		int b = 1;
		int range = 8;
		System.out.println(a);
		System.out.println(b);
		for(int i = 0 ; i < range-2 ; i++){
			int c = a + b;
			
			System.out.println(c +" ");
			a = b ;
			b = c;
		
		}*/

		//WITH RECURSION
		int ret = fibo(8);
		System.out.println(ret + " ");	
	}
}
