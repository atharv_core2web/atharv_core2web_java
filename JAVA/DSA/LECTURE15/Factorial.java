class Factorial{

	//WITHOUT RECURSION
	/*static int factorial(int num){
	
		int fact = 1;
		for(int i = 1 ; i <= num ; i++){
		
			fact *= i;
		}
		return fact;
	}*/

	//WITH RECURSION
	static int factorial(int num){
	
		if(num == 0)
			return 1;

		return num * factorial(num - 1);
	}
	public static void main(String[] s){
	
		int ret = factorial(5);
		System.out.println(ret);
	}
}
