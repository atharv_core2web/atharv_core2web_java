class MergeSort {
    static void merge(int[] arr, int start, int mid, int end) {
        // Step 1: Determine length of 2 partitions
        int n1 = mid - start + 1;
        int n2 = end - mid;
        
        // Step 2: Create temporary arrays
        int[] leftArr = new int[n1];
        int[] rightArr = new int[n2];
        
        // Step 3: Copy elements to left array
        for (int i = 0; i < n1; i++) {
            leftArr[i] = arr[start + i];
        }
        
        // Step 4: Copy elements to right array
        for (int j = 0; j < n2; j++) {
            rightArr[j] = arr[mid + 1 + j];
        }
        
        // Step 5: Merge the temporary arrays back into original array
        int i = 0, j = 0, k = start;
        
        while (i < n1 && j < n2) {
            // Fixed comparison to use rightArr[j] instead of arr[j]
            if (leftArr[i] <= rightArr[j]) {
                arr[k] = leftArr[i];
                i++;
            } else {
                arr[k] = rightArr[j];
                j++;
            }
            k++;
        }
        
        // Copy remaining elements of leftArr if any
        while (i < n1) {
            arr[k] = leftArr[i];
            i++;
            k++;
        }
        
        // Copy remaining elements of rightArr if any
        while (j < n2) {
            arr[k] = rightArr[j];
            j++;
            k++;
        }
    }
    
    static void divideArray(int[] arr, int start, int end) {
        if (start < end) {
            int mid = start + (end - start) / 2;
            
            // Divide left side
            divideArray(arr, start, mid);
            // Divide right side
            divideArray(arr, mid + 1, end);
            // Merge the divided arrays
            merge(arr, start, mid, end);
        }
    }
    
    public static void main(String[] args) {
        int[] arr = {9, 1, 8, 2, 7, 3, 6, 4};
        divideArray(arr, 0, arr.length - 1);
        
        // Print sorted array
        for (int num : arr) {
            System.out.print(num + " ");
        }
    }
}
