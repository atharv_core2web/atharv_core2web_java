class mergeSortPrac{

	static void mergeSort(int arr[],int start , int end){
	
		if(start < end){
		
			int mid = (start + end)/2;

			System.out.println("Start : " + start + " End : " + end +" Mid : "+ mid);

			mergeSort(arr,start,mid);
			mergeSort(arr,mid+1,end);
		}
	}

	public static void main(String[] s){
	
		int arr[] = new int[]{9,1,8,2,7,3,6,4};

		mergeSort(arr,0,arr.length-1);
	}
}
