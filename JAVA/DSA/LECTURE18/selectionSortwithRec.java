class selectionSortwithRec{

        static void sort(int arr[],int i){

                int n = arr.length;
		

                if(i == n - 1)
			return;

                        int minIndex = i;
                        for(int j = i + 1 ; j < n ; j++){
                                if(arr[j] < arr[minIndex])
                                        minIndex = j;
                        }
                        int temp = arr[i];
                        arr[i] = arr[minIndex];
                        arr[minIndex] = temp;

                sort(arr,i + 1);
        }

        public static void main(String[] s){

                int arr[] = new int[]{9 , 2 , 1 ,7 , 10};
                sort(arr , 0);
                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print(arr[i] + " ");
                }
        }
}
