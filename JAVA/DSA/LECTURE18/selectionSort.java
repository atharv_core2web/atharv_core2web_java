class selectionSort{

	static void sort(int arr[]){
	
		int n = arr.length;

		for(int i = 0 ; i < n - 1; i++){
			
			int minIndex = i;
			for(int j = i + 1 ; j < n ; j++){
				if(arr[j] < arr[minIndex])
					minIndex = j;
			}
			int temp = arr[i];
			arr[i] = arr[minIndex];
			arr[minIndex] = temp;
		
		}
	}

	public static void main(String[] s){

		int arr[] = new int[]{9 , 2 , 1 ,7 , 10};
		sort(arr);
		for(int i = 0 ; i < arr.length ; i++){
	
			System.out.print(arr[i] + " ");
		}
	}
}


/*
 
   if(n == arr.length - 1)
	sort(arr,n-1)

 * /
