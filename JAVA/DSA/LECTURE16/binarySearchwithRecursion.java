import java.util.*;

class binarySearchwithRecursion{

	static int binarySearch(int arr[] , int start , int end , int ele){
		
		if(start > end){
			return -1;
		}else{
			int mid = (start + end) / 2;

			if(arr[mid] == ele)
				return mid;

			if(arr[mid] < ele)
				return binarySearch(arr , mid + 1 , end , ele);
			else	
				return binarySearch(arr , start , mid - 1 , ele);
		}
	}

	public static void main(String[] s){

		Scanner sc = new Scanner(System.in);
                int arr[] = new int[]{4,7,11,24,35,57,75,87};
                int ele = sc.nextInt();

		int start = 0;
		int end = arr.length - 1;
                int result = binarySearch(arr,start,end,ele);
                if(result == -1)
                        System.out.println("Not Found");
                else
                        System.out.println("Found at index " + result);

        }
}
