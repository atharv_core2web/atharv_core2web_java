class binarySearch{

	static int search(int arr[],int ele){
	
		int n = arr.length;

		int start = 0;
		int end = n - 1;
		int cnt = 0;

		while(start <= end){
			cnt++;
			int mid = (start + end) / 2;
			
			if(arr[mid] == ele)
			       return mid;
			if(arr[mid] > ele)
				end = mid - 1;
			if(arr[mid] < ele)
				start = mid + 1;
		}
		

		return -1;
		
	}

	public static void main(String[] s){
	
		int arr[] = new int[]{4,7,11,24,35,57,75,87};
		int ele = 5;

		int result = search(arr,ele);
		if(result == -1)
			System.out.println("Not Found");
		else
			System.out.println("Found at index " + result);

	}
}
