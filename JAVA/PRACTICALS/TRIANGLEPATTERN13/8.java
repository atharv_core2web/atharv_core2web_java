import java.io.*;

class Program8{
        public static void main(String[] args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader (System.in));
                System.out.print("Enter Rows : ");
                int row = Integer.parseInt(br.readLine());
                for(int i = 1; i <= row ; i++){
                        int ch = 65 + row - i;
                        int num = row - i + 1 ;
                        for(int j = i ; j <= row ; j++){
				if(i % 2 == 0){
					System.out.print((char)ch + " ");
					ch--;
				}
				else{
					System.out.print(num + " ");
					num--;
				}
			}
			System.out.println();
		}
	}
}
